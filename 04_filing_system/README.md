# Ablagekonzept <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Ziele](#1-ziele)
- [2. IST-Zustand: Wo speichere ich welche Daten ab?](#2-ist-zustand-wo-speichere-ich-welche-daten-ab)
  - [2.1. Vorgehen](#21-vorgehen)
- [3. Das eigene Ablagekonzept grafisch darstellen](#3-das-eigene-ablagekonzept-grafisch-darstellen)
  - [3.1. Vorgehen](#31-vorgehen)
  - [3.2. Beispiel](#32-beispiel)
- [4. Das eigene Ablagekonzept analysieren](#4-das-eigene-ablagekonzept-analysieren)
- [5. Weiterführende Links und Quellen](#5-weiterführende-links-und-quellen)

# 1. Ziele
 - Kann zwischen allgemeinen und Personen bezogene Daten unterscheiden.
 - B1G: Kann den Grundsatz Daten und Applikation zu trennen erläutern
 - B1F: Wendet ein vorgegebenes Ablagekonzept an, um die eigenen Daten zu organisieren und zu speichern.
 - B1E: Entwirft ein eigenes Ablagekonzept für seine Daten und setzt dieses um.
 - B2G: Kann den Unterschied zwischen lokaler und cloudbasierter Ablage erläutern.
 - B2F: Richtet und setzt lokale und cloudbasierte Ablagen ein.
 - B2E: Vergleicht verschiedene cloudbasierte Ablagen.


# 2. IST-Zustand: Wo speichere ich welche Daten ab?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Auflistung von allen Gerätenm, Services und darauf abgespeicherten Daten |

Jeder von uns verwendet verschiedene Geräte (Notebooks, Smartphone) und hat darauf persönliche Daten abgespeichert. In dieser Übung halten Sie fest auf welchem Gerät, Sie welche Informationen gespeichert haben und welche Services Sie verwenden. 
Lassen Sie bei Ihrer Auflistung die Daten von Ihrem Arbeitsort weg. 

**Tipp**: Lesen Sie zuerst die nachfolgende Aufträge. Eventuell fällt es Ihnen einfacher mit der grafischen Darstellung zu beginnen. 

## 2.1. Vorgehen
 1. Erstellen Sie eine Tabelle mit all ihren persönlichen Mikrocomputern (Notebook, Smartphone, usw.) mit den wichtigsten Spezifikationen (Individueller Name, Marke, Typ, Speichergrösse, usw...) und deren Verwendungszweck. 
 2. Erstellen Sie eine Tabelle mit allen Apps/Services, die mindestens eine der folgenden Kriterien erfüllen: <br> (A) Dient zur Kommunikation oder Kollaboration<br>(B) Beinhaltet personenbezogene Daten, die Sie selbst freigegeben / eingefügt haben. <br>Notieren Sie zu jedem App/Service, welche persönliche Daten diese beinhalten. 
 3. Erstellen Sie eine Tabelle mit allen Speichermedien auf denen Sie Daten abspeichern. (z.B. den Ordner DCIM mit Fotos auf Ihrem Smartphone). 

Halten Sie alle diese Informationen in Ihrem persönlichen Portfolio fest. 

**Tipp:** Lassen Sie Apps, die nur allgemeine Personenbezogene Daten beinhalten (z.B. Name + Address für Lieferdienste) weg oder fassen Sie diese zusammen. 

# 3. Das eigene Ablagekonzept grafisch darstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  4 Lektion |
| Ziele | Übersichtsgrafik des persönliches Ablagekonzept erstellt. |

Mithilfe der Informationen aus der vorhergehenden Übung können Sie nun Ihr Ablagekonzept entwickeln. 

## 3.1. Vorgehen
 1. Wählen Sie ein vektorbasiertes Zeichnungsprogramm ([draw.io](draw.io), Microsoft Visio, o.ä.) aus.
 2. Stellen Sie nun die Daten aus der vorherigen Übung grafisch dar.

**Lassen Sie allgemeine Services (Onlineshop, Spiele, usw.), die nur Kontaktdaten enthalten, in Ihrer Grafik weg.**

## 3.2. Beispiel
Die Darstellungsart steht Ihnen frei. Zur Inspiration können diese Grafiken dienen:
![Beispiel grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_1.png)

![Beispiel grafische Darstellung persönliches Ablagekonzept](images/ablagekonzept_beispiel_2.png)



# 4. Das eigene Ablagekonzept analysieren
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Daten nach schutzwürdigkeit einstufen |

In der oben dargestellten Grafik wurden die Datenkategorien und Services teilweise mit Symbolen versehen, die ihre Schutzwürdigkeit definieren. Grundsätzlich gilt es eine Unterscheidung zwischen allgemeinen Daten und schutzwürdigen Personendaten zu machen. Zusätzlich machen wir noch eine Unterscheidung zwischen eigenen und fremden Personendaten. Den während wir das Recht über unsere eigenen Personendaten haben, so müssen wir bei Personendaten von Dritten uns an die Vorgaben des Datenschutzgesetzes halten. Wir definieren die folgende Kategorien:
 - Allgemeine Daten
 - Schützenswerte eigene Personendaten
 - Schützenswerte Personendaten von Dritten

Teilen Sie nun alle Ihre Datenablagen und verwendeten Services in diese drei Kategorien ein. 

# 5. Weiterführende Links und Quellen
 - https://medium.com/golden-data/what-is-a-filing-system-under-eu-data-protection-law-6e7222743f71
 - https://www.data.cam.ac.uk/data-management-guide/organising-your-data
 - https://www.asianefficiency.com/organization/organize-your-files-folders-documents/